package indexWikiElasticsearch;

import java.util.Properties;
import configuration.ConfigurationManager;
import indexing.*;

public class Main {
	/**
	 * Path where the initial configuration file is stored
	 */
	private final static String CONFIG_PATH = "config.properties";

	public static void main(String[] args) {
		try {
			// reading the properties stored in the configuration file
			Properties properties = ConfigurationManager.getInstance().getProperties(CONFIG_PATH);
			String dumpPath = properties.getProperty("dumppath");
			String indexPath = properties.getProperty("indexpath");

			// the actual indexing 
			long startTime = System.currentTimeMillis();

			System.out.println("Indexing started..");

			WikiIndexer indexer = new WikiElasticsearchIndexer(dumpPath, indexPath);
			indexer.startIndexing();

			System.out.println("Indexing done.!");

			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			System.out.println(elapsedTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}