package indexing;

import parsing.WikiDumpParser;
import parsing.WikiPage;

/**
 * An abstract class that provides an interface to index the wiki xml dump
 * 
 * @author aiham
 */
public abstract class WikiIndexer {
	/**
	 * parses the wiki xml dump
	 */
	protected WikiDumpParser wikiParser;

	/**
	 * the path in which the index data will be stored
	 */
	protected String indexPath;

	/**
	 * the path of the xml wiki dump
	 */
	protected String wikiDumpPath;

	/**
	 * Constructs an instance from the WikiIndexer class
	 * 
	 * @param wikiDumpPath
	 *            represents the path in which the input dump is stored
	 * @param indexPath
	 *            represents the path in which the output index file(s) is
	 *            stored
	 */
	public WikiIndexer(String wikiDumpPath, String indexPath) {
		// necessary checks of the parameters
		if (indexPath == null || indexPath.isEmpty()) {
			throw new IllegalArgumentException("Index path cannot be null or empty");
		}

		if (wikiDumpPath == null || wikiDumpPath.isEmpty()) {
			throw new IllegalArgumentException("The given xml dump path is empty or null");
		}

		this.indexPath = indexPath;
		this.wikiDumpPath = wikiDumpPath;
		this.wikiParser = new WikiDumpParser(this.wikiDumpPath, this);
	}

	/**
	 * initiates the indexing process: by calling the wiki parser which manages
	 * the next steps
	 * 
	 * @throws Exception
	 */
	public void startIndexing() throws Exception {
		this.wikiParser.startParsing();
		this.finalizeIndexing();
	}

	/**
	 * ends the indexing process, its implementation differs according to the
	 * implementing class
	 * 
	 * @throws Exception
	 */
	protected abstract void finalizeIndexing() throws Exception;

	/**
	 * interface method that indexes an individual wikipage
	 * 
	 * @param wikiPage
	 *            the input wikipage
	 */
	public abstract void indexWikiPage(WikiPage wikiPage);
}
