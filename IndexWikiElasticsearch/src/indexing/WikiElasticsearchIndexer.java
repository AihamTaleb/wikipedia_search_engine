package indexing;

import parsing.WikiPage;
import java.io.IOException;
import static org.elasticsearch.node.NodeBuilder.*;
import static org.elasticsearch.common.xcontent.XContentFactory.*;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.node.Node;

/**
 * An implementing class that uses elasticsearch framework as an indexing tool
 * 
 * @author aiham
 */
public class WikiElasticsearchIndexer extends WikiIndexer {

	/**
	 * stores the name of the local elastic search cluster for indexing and
	 * later for lookup
	 */
	private final static String CLUSTER_NAME = "aiham-wiki-elasticsearch";

	/**
	 * name of the index
	 */
	private final static String INDEX_NAME = "wikipedia";

	/**
	 * name of the type inside the index
	 */
	private final static String TYPE_NAME = "wikipage";

	/**
	 * number of requests allowed to accumulate, after this limit the bulk will
	 * be flushed (executed)
	 */
	private final static int NUM_BULK_WIKIPAGES = 1000;

	/**
	 * stores the instance of the node inside this cluster
	 */
	private Node innerNode;

	/**
	 * represents a client that interacts with the defined node
	 */
	private Client innerClient;

	/**
	 * instance of the bulk processor which makes use of the bulk API: this aims
	 * at reducing the total number of indexing requests to the server
	 */
	private BulkProcessor bulkProcessor;

	/**
	 * initializes an instance of WikiElasticsearchIndexer class: it starts the
	 * processing active node, creates a connection client, defines the mapping
	 * in the index, and initializes the bulk processor object
	 * 
	 * @param wikiDumpPath
	 *            the path to the input wiki dump
	 * @param indexPath
	 *            the output path of the created index
	 */
	public WikiElasticsearchIndexer(String wikiDumpPath, String indexPath) {
		super(wikiDumpPath, indexPath);

		// init the running node
		this.innerNode = nodeBuilder().clusterName(CLUSTER_NAME)
				.settings(ImmutableSettings.settingsBuilder().put("path.data", this.indexPath)).local(true).node();

		// init the connection client
		this.innerClient = innerNode.client();
		try {
			// define the corresponding mapping
			XContentBuilder indexMapping = jsonBuilder().startObject().startObject(TYPE_NAME).startObject("properties")
					.startObject("title").field("type", "string").field("index", "not_analyzed").endObject()
					.startObject("content").field("type", "string").field("analyzer", "english").endObject().endObject()
					.endObject().endObject();

			// create the index and set the mapping of our wiki type in it
			this.innerClient.admin().indices().prepareCreate(INDEX_NAME).addMapping(TYPE_NAME, indexMapping).execute()
					.actionGet();

		} catch (IOException e) {
			e.printStackTrace();
		}

		// init the bulk processor & override the abstract methods
		this.bulkProcessor = BulkProcessor.builder(this.innerClient, new BulkProcessor.Listener() {
			@Override
			public void beforeBulk(long executionId, BulkRequest request) {
			}

			@Override
			public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
			}

			@Override
			public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
			}
		}).setBulkActions(NUM_BULK_WIKIPAGES).setConcurrentRequests(1).build();
	}

	@Override
	public void indexWikiPage(WikiPage wikiPage) {
		try {

			// creating the json document source to be stored in the index
			XContentBuilder indexSource = jsonBuilder().startObject().field("title", wikiPage.getPageTitle())
					.field("content", wikiPage.getPageContent()).endObject();

			// adding the created json doc to the bulk request
			this.bulkProcessor.add(this.innerClient.prepareIndex(INDEX_NAME, TYPE_NAME, wikiPage.getPageId())
					.setSource(indexSource).request());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * the finalizing of indexing in WikiElasticsearchIndexer class means
	 * closing the running node and the connected client
	 */
	@Override
	protected void finalizeIndexing() throws Exception {
		this.innerClient.close();
		this.innerNode.close();
	}
}
