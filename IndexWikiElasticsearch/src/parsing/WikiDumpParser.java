package parsing;

import org.xml.sax.SAXException;
import indexing.WikiIndexer;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Parses the Wikipedia dump using XML SAX Parser.
 */
public class WikiDumpParser {

	/**
	 * path to the wiki xml dump
	 */
	private String wikiDumpPath;

	/**
	 * reference to the corresponding indexer instance (to be passed to the SAX
	 * parser)
	 */
	private WikiIndexer correspondingIndexer;

	/**
	 * This is the constructor for WikiParse
	 * 
	 * @param wikiDumpPath
	 *            This is the path of the XML file to be parsed The document
	 *            needs to be a proper Wikipedia dump.
	 */
	public WikiDumpParser(String wikiDumpPath, WikiIndexer indexer) {
		this.wikiDumpPath = wikiDumpPath;
		this.correspondingIndexer = indexer;
	}

	/**
	 * Initiation of the parsing process, which is performed page by page using
	 * the SAX parser.
	 */
	public void startParsing() {
		try {
			// initializing the SAX parser
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			SAXParserHandler saxParserHandler = new SAXParserHandler(this.correspondingIndexer);

			saxParser.parse(wikiDumpPath, saxParserHandler);

		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

	}
}
