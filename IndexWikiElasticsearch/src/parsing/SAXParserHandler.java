package parsing;

import parsing.WikiPageContentAnalyzer;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import indexing.WikiIndexer;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Uses SAX parser to parse the wiki XML dump file. It parses the file
 * iteratively and stores the contents of the currently processed XML tag each
 * time only
 * 
 * @author aiham
 */
public class SAXParserHandler extends DefaultHandler {

	/**
	 * This stack is used to store the sequence of the XML tags
	 */
	private Deque<String> tagStack;

	/**
	 * This string builder will be used to store the data between the tags
	 */
	private StringBuilder currentTagContents;

	/**
	 * Variable for WikiPageConcise that save basic information for a Wiki Page
	 */
	private WikiPage wikiPage;

	/**
	 * reference to the corresponding indexer
	 */
	private WikiIndexer referencedIndexer;

	/**
	 * determines whether the processed page is a redirect or not: redirect
	 * pages have no content, so they are ignored in our solution
	 */
	private boolean redirectPage;

	/**
	 * counts the overall number of processed pages so far
	 */
	private long numProcesedPages;

	/**
	 * initializes the SAX parser handler
	 * 
	 * @param indexer
	 *            a reference to the corresponding indexer in order to send each
	 *            parsed wikipage to it to be indexed
	 */
	public SAXParserHandler(WikiIndexer indexer) {
		this.tagStack = new LinkedList<String>();
		this.currentTagContents = new StringBuilder();
		this.redirectPage = false;
		this.referencedIndexer = indexer;
		this.numProcesedPages = 0;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if (qName.equalsIgnoreCase("page")) {
			// creating a wikipage to hold the contents of the page tag
			this.wikiPage = new WikiPage();
			this.redirectPage = false;
		} else if (qName.equalsIgnoreCase("redirect")) {
			// the existence of a redirect element in the wikipage indicates
			// that it's a redirect page
			this.redirectPage = true;
		}

		// empty the tag contents holder
		this.currentTagContents.setLength(0);

		this.tagStack.push(qName);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// popping the latest tag so far
		String endTag = this.tagStack.pop();

		// represents the parent surrounding tag
		String parentTag = null;

		if (!this.tagStack.isEmpty())
			parentTag = this.tagStack.peek();

		if (endTag.equalsIgnoreCase("page") && !this.redirectPage) {
			// actual parsing of the wikipage content using the
			// WikiPageContentAnalyzer object
			String analyzedPageContent = WikiPageContentAnalyzer.process(this.wikiPage.getPageContent());
			this.wikiPage.setPageContent(analyzedPageContent);

			// indexing the parsed and analyzed page content
			this.referencedIndexer.indexWikiPage(this.wikiPage);

			this.numProcesedPages++;
			if (this.numProcesedPages % 200 == 0) {
				System.out.println(this.numProcesedPages);
			}

		} else if (endTag.equalsIgnoreCase("title")) {
			// setting the page title
			this.wikiPage.setPageTitle(new String(this.currentTagContents).toLowerCase());
		} else if (endTag.equalsIgnoreCase("id") && parentTag.equalsIgnoreCase("page")) {
			// setting the wikipage id
			this.wikiPage.setPageId(this.currentTagContents.toString());
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		this.currentTagContents.append(new String(ch, start, length));
	}

}