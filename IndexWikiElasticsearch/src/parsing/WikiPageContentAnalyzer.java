package parsing;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Analyzes and parses the wiki xml page contents, it keeps and discards
 * specific page sections
 * 
 * @author aiham
 */
public class WikiPageContentAnalyzer {

	/**
	 * processes and analyzes the wiki page contents string
	 * 
	 * @param wikiPgContent
	 * @return
	 */
	public static String process(String wikiPgContent) {
		int wikiContentLength = wikiPgContent.replace("\n", "").replace("\r", "").length();

		// to hold the output processed content
		StringBuilder stringBuilder = new StringBuilder();

		// to store the contents of the infobox section (if it exists in the
		// input page)
		String infoboxText = null;

		// analyzing the content chars
		int charIndex = 0;
		for (; charIndex < wikiContentLength; charIndex++) {
			char currentChar = wikiPgContent.charAt(charIndex);

			if (Character.isLetterOrDigit(currentChar) || Character.isWhitespace(currentChar)) {
				// keeping the alphanumerics and whitespaces (they are all parts
				// of regular words)
				stringBuilder.append(currentChar);

			} else if (currentChar == '{' && charIndex + 20 < wikiContentLength) {
				// working with special sections that start with curly braces

				if (wikiPgContent.substring(charIndex + 1, charIndex + 9).equals("{infobox")) {

					// Infoboxes are appended in the end of the page content.
					StringBuilder infoboxString = new StringBuilder();
					charIndex = fillInfoboxString(wikiPgContent, wikiContentLength, charIndex, infoboxString);
					infoboxText = processInfobox(infoboxString);
				} else if (wikiPgContent.substring(charIndex + 1, charIndex + 8).equals("{geobox")
						|| wikiPgContent.substring(charIndex + 1, charIndex + 6).equals("{cite")
						|| wikiPgContent.substring(charIndex + 1, charIndex + 4).equals("{gr")
						|| wikiPgContent.substring(charIndex + 1, charIndex + 7).equals("{coord")
						|| wikiPgContent.substring(charIndex + 1, charIndex + 20).equals("| class=\"wikitable\"")
						|| wikiPgContent.substring(charIndex + 1, charIndex + 15).equals("| cellspacing=")) {

					// Geoboxes & Citations & GRs & Coords & wikitables are
					// removed.
					charIndex = removeContentCurlyBraces(wikiPgContent, wikiContentLength, charIndex);
				}

			} else if (currentChar == '[' && charIndex + 11 < wikiContentLength) {
				// working with special sections that start with square brackets

				if (wikiPgContent.substring(charIndex + 1, charIndex + 11).equals("[category:")) {

					// Categoies are kept, but non-alphanumeric chars are
					// removed from them.
					StringBuilder categoryString = new StringBuilder();
					charIndex = fillCategoryString(wikiPgContent, wikiContentLength, charIndex, categoryString);
					for (int catCharIndex = 0; catCharIndex < categoryString.length(); catCharIndex++) {
						char currentCatChar = categoryString.charAt(catCharIndex);
						if (Character.isLetter(currentCatChar) || Character.isWhitespace(currentCatChar))
							stringBuilder.append(currentCatChar);
					}
				} else if (wikiPgContent.substring(charIndex + 1, charIndex + 8).equals("[image:")
						|| wikiPgContent.substring(charIndex + 1, charIndex + 7).equals("[file:")) {

					// Images & Files are removed
					charIndex = removeContentSqrBrackets(wikiPgContent, wikiContentLength, charIndex);
				}

			} else if (currentChar == '<' && charIndex + 8 < wikiContentLength) {
				// working with special sections that start with angle brackets

				String closingTag = null;
				if (wikiPgContent.substring(charIndex + 1, charIndex + 4).equals("!--")) {
					closingTag = "-->";
				} else if (wikiPgContent.substring(charIndex + 1, charIndex + 4).equals("ref")) {
					closingTag = "</ref>";
				} else if (wikiPgContent.substring(charIndex + 1, charIndex + 8).equals("gallery")) {
					closingTag = "</gallery>";
				}

				// Comments & References & Galleries are removed
				charIndex = removeContentAngleBrackets(wikiPgContent, wikiContentLength, charIndex, closingTag);

			} else if (currentChar == '=' && (charIndex + 1) < wikiContentLength
					&& wikiPgContent.charAt(charIndex + 1) == '=') {
				// the sections that start with == (such as subtitles, external
				// links, .. etc)

				charIndex += 1;
				while (charIndex < wikiContentLength && (currentChar == ' ' || currentChar == '\t')) {
					currentChar = wikiPgContent.charAt(charIndex);
					charIndex++;
				}

				// when reaching the "external links" or "see also" or
				// "references" or "further reading" sections, stop the parsing
				// process, as they are always in the end of the page
				if (charIndex + 15 < wikiContentLength) {
					if ((wikiPgContent.substring(charIndex + 1, charIndex + 15).equals("external links"))
							|| (wikiPgContent.substring(charIndex + 1, charIndex + 9).equals("see also"))
							|| (wikiPgContent.substring(charIndex + 1, charIndex + 11).equals("references"))
							|| (wikiPgContent.substring(charIndex + 1, charIndex + 16).equals("further reading"))) {
						break;
					}
				}
			}
		}

		// appending the infobox contents to the end of the returned content
		if (infoboxText != null)
			stringBuilder.append(infoboxText);

		return stringBuilder.toString();
	}

	/**
	 * processes the category section and takes special care of it
	 * 
	 * @param wikiContent
	 *            the wiki page content
	 * @param wikiContentLength
	 * @param charIndex
	 * @param categoryString
	 *            the contents of the category section only
	 * @return
	 */
	private static int fillCategoryString(String wikiContent, int wikiContentLength, int charIndex,
			StringBuilder categoryString) {
		char currentChar;
		int count = 0;
		for (; charIndex < wikiContentLength; charIndex++) {

			currentChar = wikiContent.charAt(charIndex);
			categoryString.append(currentChar);
			if (currentChar == '[') {
				count++;
			} else if (currentChar == ']') {
				count--;
			}
			if (count == 0 || (currentChar == '=' && charIndex + 1 < wikiContentLength
					&& wikiContent.charAt(charIndex + 1) == '=')) {
				if (currentChar == '=') {
					categoryString.deleteCharAt(categoryString.length() - 1);
				}
				charIndex--;
				break;
			}
		}
		return charIndex;
	}

	/**
	 * processes the special case of the infobox contents: it extracts the
	 * useful information from it and removes the undesired non-alphanumeric
	 * chars
	 * 
	 * @param infoboxString
	 *            the infobox contents
	 * @return parsed and analyzed infobox contents string
	 */
	private static String processInfobox(StringBuilder infoboxString) {

		HashMap<String, String> infoHash = new HashMap<String, String>();
		StringBuilder keyBuilder = new StringBuilder();
		StringBuilder valueBuilder = new StringBuilder();
		int infoboxStringLength = infoboxString.length();
		char currentChar = '\0';
		int charIndex = 9;
		for (; charIndex < infoboxStringLength; charIndex++) {
			currentChar = infoboxString.charAt(charIndex);

			if (currentChar == '|') {
				for (charIndex++; charIndex < infoboxStringLength; charIndex++) {
					currentChar = infoboxString.charAt(charIndex);
					if (currentChar == '=') {
						break;
					}
					keyBuilder.append(currentChar);
				}

				int count = 0;
				boolean isReplaced = false;
				for (charIndex++; charIndex < infoboxStringLength; charIndex++) {
					currentChar = infoboxString.charAt(charIndex);
					if (currentChar == '[' || currentChar == '{' || currentChar == '(') {
						count++;
					} else if (currentChar == ']' || currentChar == '}' || currentChar == ')') {
						count--;
					} else if (currentChar == '<') {

						if (charIndex + 4 < infoboxStringLength
								&& infoboxString.substring(charIndex + 1, charIndex + 4).equals("!--")) {
							String closingTag = "-->";
							charIndex = removeContentAngleBrackets(infoboxString.toString(), infoboxStringLength,
									charIndex, closingTag);
							isReplaced = true;
						}
					} else if (count == 0 && currentChar == '|') {
						charIndex--;
						break;
					}

					if (!isReplaced)
						valueBuilder.append(currentChar);
				}

				if (keyBuilder.length() > 0) {
					String value = valueBuilder.toString().trim();
					if (value.length() > 0)
						infoHash.put(keyBuilder.toString().trim(), value);
				}

				keyBuilder.setLength(0);
				valueBuilder.setLength(0);
			}

		}

		if (keyBuilder.length() > 0) {
			String value = new String(valueBuilder).trim();
			if (value.length() > 0)
				infoHash.put(new String(keyBuilder).trim(), value);
		}

		StringBuilder infoboxTextRepr = new StringBuilder();
		Iterator<String> itr = infoHash.keySet().iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			String value = infoHash.get(key);

			infoboxTextRepr.append(key);
			infoboxTextRepr.append(":");
			infoboxTextRepr.append(value);
			infoboxTextRepr.append('\n');

		}
		infoboxTextRepr.append(":\n");
		return infoboxTextRepr.toString();
	}

	/**
	 * extracts the infobox contents and fills them in the stringbuilder holder
	 * 
	 * @param wikiContent
	 *            the wikipage content
	 * @param wikiContentLength
	 *            the length of this content
	 * @param charIndex
	 *            the current char reached in parsing
	 * @param infoboxString
	 *            the output stringbuilder which will contain the infobox
	 *            contents
	 * @return the updated charIndex
	 */
	private static int fillInfoboxString(String wikiContent, int wikiContentLength, int charIndex,
			StringBuilder infoboxString) {

		int count = 0;
		for (; charIndex < wikiContentLength; charIndex++) {
			char currentChar = wikiContent.charAt(charIndex);
			infoboxString.append(currentChar);
			if (currentChar == '{') {
				count++;
			} else if (currentChar == '}') {
				count--;
			}
			if (count == 0 || (currentChar == '=' && charIndex + 1 < wikiContentLength
					&& wikiContent.charAt(charIndex + 1) == '=')) {
				if (currentChar == '=') {
					infoboxString.deleteCharAt(infoboxString.length() - 1);
				}
				charIndex--;
				break;
			}
		}
		return charIndex;
	}

	/**
	 * auxiliary function to remove the section contents inside curly braces
	 * 
	 * @param wikiContent
	 *            the whole wikipage contents
	 * @param wikiContentLength
	 *            the wikipage length
	 * @param charIndex
	 *            the charIndex reached by the parsing
	 * @return the updated charIndex
	 */
	private static int removeContentCurlyBraces(String wikiContent, int wikiContentLength, int charIndex) {
		if (wikiContent == null) {
			return charIndex;
		}

		int count = 0;
		for (; charIndex < wikiContentLength; charIndex++) {
			char currentChar = wikiContent.charAt(charIndex);
			if (currentChar == '{') {
				count++;
			} else if (currentChar == '}') {
				count--;
			}
			if (count == 0 || (currentChar == '=' && charIndex + 1 < wikiContentLength
					&& wikiContent.charAt(charIndex + 1) == '=')) {
				charIndex--;
				break;
			}
		}
		return charIndex;
	}

	/**
	 * auxiliary function to remove the section contents inside angle brackets
	 * 
	 * @param wikiPgContent
	 *            the whole wikipage contents
	 * @param wikiContentLength
	 *            the wikipage length
	 * @param charIndex
	 *            the charIndex reached by the parsing
	 * @param closingTag
	 *            the type of the closing tag
	 * @return the updated charIndex
	 */
	private static int removeContentAngleBrackets(String wikiPgContent, int wikiContentLength, int charIndex,
			String closingTag) {
		if (wikiPgContent == null || closingTag == null) {
			return charIndex;
		}

		int indexClosingTag = wikiPgContent.indexOf(closingTag, charIndex + 1);
		if (indexClosingTag == -1 || indexClosingTag + closingTag.length() > wikiContentLength) {
			charIndex = wikiContentLength - 1;
		} else {
			charIndex = indexClosingTag + closingTag.length() - 1;
		}
		return charIndex;
	}

	/**
	 * auxiliary function to remove the section contents inside square brackets
	 * 
	 * @param wikiContent
	 *            the whole wikipage contents
	 * @param wikiContentLength
	 *            the wikipage length
	 * @param charIndex
	 *            the charIndex reached by the parsing
	 * @return the updated charIndex
	 */
	private static int removeContentSqrBrackets(String wikiContent, int wikiContentLength, int charIndex) {
		if (wikiContent == null) {
			return charIndex;
		}

		char currentChar;
		int count = 0;
		for (; charIndex < wikiContentLength; charIndex++) {

			currentChar = wikiContent.charAt(charIndex);

			if (currentChar == '[') {
				count++;
			} else if (currentChar == ']') {
				count--;
			}
			if (count == 0 || (currentChar == '=' && charIndex + 1 < wikiContentLength
					&& wikiContent.charAt(charIndex + 1) == '=')) {
				charIndex--;
				break;
			}
		}
		return charIndex;
	}

}