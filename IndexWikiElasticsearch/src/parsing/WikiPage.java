package parsing;

/**
 * the model that stores the necessary wikipage contents
 * 
 * @author aiham
 *
 */
public class WikiPage {

	/**
	 * the wikipage identifier
	 */
	private String _pageId;

	/**
	 * the wikipage title
	 */
	private String _pageTitle;

	/**
	 * holds the contents of the wikipage, usually stored after analysis
	 */
	private String _pageContent;

	public String getPageId() {
		return _pageId;
	}

	public void setPageId(String pageId) {
		this._pageId = pageId;
	}

	public String getPageTitle() {
		return _pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this._pageTitle = pageTitle;
	}

	public String getPageContent() {
		return _pageContent;
	}

	public void setPageContent(String pageText) {
		this._pageContent = pageText;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("PageID: " + this._pageId + "\n");
		sb.append("Page Title: " + this._pageTitle + "\n");
		sb.append("Page Content: \n");
		sb.append(this._pageContent);
		sb.append("\n\n\n");

		return sb.toString();
	}
}
