package configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Reads and manages all project specific configurations (properties). These are
 * written in the config.properties file which should be located just next to
 * the jar executable file.
 * 
 * @author aiham
 */
public class ConfigurationManager {

	/**
	 * Singleton pattern: ensures that only one instance is created during the
	 * whole runtime
	 */
	private static ConfigurationManager singletonInstance;

	/**
	 * private constructor to prevent initializing more than one instance of
	 * this class
	 */
	private ConfigurationManager() {

	}

	/**
	 * gets the initialized singleton instance
	 */
	public static ConfigurationManager getInstance() {
		if (singletonInstance == null) {
			singletonInstance = new ConfigurationManager();
		}

		return singletonInstance;
	}

	/**
	 * gets the properties object stored in the configuration file
	 * 
	 * @param configFilePath
	 *            represents the path in which the configuration file is stored
	 * @return the properties object that holds the information in the config
	 *         file
	 * @throws IOException
	 *             in case the config file doesn't exist or a problem occurred
	 *             while openning it
	 */
	public Properties getProperties(String configFilePath) throws IOException {
		Properties prop = new Properties();
		InputStream input;
		try {
			input = new FileInputStream(configFilePath);
		} catch (FileNotFoundException ex) {
			throw new FileNotFoundException("config.properties file should be located next to the executable file");
		}

		// load a properties file
		prop.load(input);

		return prop;
	}
}
