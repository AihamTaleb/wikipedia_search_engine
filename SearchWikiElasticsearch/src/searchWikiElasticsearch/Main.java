package searchWikiElasticsearch;

import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import configuration.ConfigurationManager;
import search.ElasticsearchSearcher;
import search.WikiSearcher;

public class Main {

	/**
	 * Path where the initial configuration file is stored
	 */
	private final static String CONFIG_PATH = "config.properties";

	public static void main(String[] args) {
		try {
			// reading the properties stored in the configuration file
			Properties properties = ConfigurationManager.getInstance().getProperties(CONFIG_PATH);
			String indexPath = properties.getProperty("indexpath");

			// initializing the wikisearcher which will use the index file(s)
			// stored in indexPath
			WikiSearcher searcher = new ElasticsearchSearcher(indexPath);

			// loop to take search queries continuously until the user enters -1
			Scanner reader = new Scanner(System.in);
			String input = "";
			while (true) {
				System.out.println("Enter your query: (enter -1 to stop the program)");
				input = reader.nextLine();

				if (input.trim().equals("-1")) {
					break;
				}

				List<String> results = searcher.searchIndex(input);
				for (String result : results) {
					System.out.println(result);
				}
			}

			// the program stopped, finalizing search
			reader.close();
			searcher.finalizeSearch();

			System.out.println("Search tool ended!");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}