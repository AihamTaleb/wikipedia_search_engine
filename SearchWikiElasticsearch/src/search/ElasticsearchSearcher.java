package search;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.node.Node;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import static org.elasticsearch.node.NodeBuilder.*;

/**
 * An implementing class that uses elasticsearch framework as a search tool in
 * its created index
 * 
 * @author aiham
 */
public class ElasticsearchSearcher extends WikiSearcher {
	/**
	 * stores the name of the local elastic search cluster for indexing and
	 * later for lookup
	 */
	private final static String CLUSTER_NAME = "aiham-wiki-elasticsearch";

	/**
	 * name of the index
	 */
	private final static String INDEX_NAME = "wikipedia";

	/**
	 * name of the type inside the index
	 */
	private final static String TYPE_NAME = "wikipage";

	/**
	 * stores the instance of the node inside this cluster
	 */
	private Node innerNode;

	/**
	 * represents a client that interacts with the defined node
	 */
	private Client innerClient;

	/**
	 * creates an instance from the elasticsearcher class
	 * 
	 * @param indexPath
	 *            the path where the index is stored, mainly it refers to the
	 *            path of the elasticsearch cluster
	 */
	public ElasticsearchSearcher(String indexPath) {
		super(indexPath);

		// initializing the running elasticsearch node
		this.innerNode = nodeBuilder().clusterName(CLUSTER_NAME)
				.settings(ImmutableSettings.settingsBuilder().put("path.data", this.indexPath)).local(true).node();

		// creating a connected client to the node
		this.innerClient = innerNode.client();

		try {
			// this sleep is performed in the beginning of the program so that
			// we give the cluster some time to run all the required shards
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<String> searchIndex(String query) {
		query = query.toLowerCase();

		long startTime = System.currentTimeMillis();

		// searching the index, here using the match query to retrieve the top
		// ranked 50 pages
		SearchResponse response = this.innerClient.prepareSearch(INDEX_NAME).setTypes(TYPE_NAME)
				.setQuery(QueryBuilders.matchQuery("content", query).analyzer("english")).setSize(50).execute()
				.actionGet();

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("search took " + elapsedTime + " ms");
		System.out.println("===result list: ===");

		SearchHit[] results = response.getHits().hits();

		if (results.length > 0) {
			List<String> titlesList = new LinkedList<String>();
			for (SearchHit hit : results) {
				Map<String, Object> result = hit.getSource(); // the retrieved
																// document
				String title = (String) result.get("title");
				titlesList.add(title + " ## score = " + hit.score());
			}

			return titlesList;
		} else {
			return new LinkedList<String>();
		}
	}

	@Override
	public void finalizeSearch() {
		this.innerClient.close();
		this.innerNode.close();
	}
}
