package search;

import java.util.List;

/**
 * An abstract class that provides an interface for searching inside the wiki
 * xml dump
 * 
 * @author aiham
 */
public abstract class WikiSearcher {

	/**
	 * the path in which the index data will be stored
	 */
	protected String indexPath;

	/**
	 * constructs an instance from the wiki searcher class
	 * 
	 * @param indexPath
	 */
	public WikiSearcher(String indexPath) {
		if (indexPath == null || indexPath.isEmpty()) {
			throw new IllegalArgumentException("Index path cannot be null or empty");
		}

		this.indexPath = indexPath;
	}

	/**
	 * searches in the predefined index, the implementation depends on the
	 * search and indexing tool
	 * 
	 * @param query
	 *            the search query string
	 * @return the list of results (wikipedia articles titles)
	 */
	public abstract List<String> searchIndex(String query);

	/**
	 * ends the search session, also the implementation depends on the tool
	 */
	public abstract void finalizeSearch();
}
